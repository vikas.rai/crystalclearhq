﻿namespace CrystalClearHQ
{
    partial class frmTradingPartnerProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbCertificateSubjName = new System.Windows.Forms.ComboBox();
            this.cmbCryptographicServiceProvider = new System.Windows.Forms.ComboBox();
            this.cmbCertificateStoreName = new System.Windows.Forms.ComboBox();
            this.cmbCertificateStoreLocation = new System.Windows.Forms.ComboBox();
            this.cmbSigningHash = new System.Windows.Forms.ComboBox();
            this.cmbEncryptionAlgorithm = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtContentType = new System.Windows.Forms.TextBox();
            this.ck997AckRequest = new System.Windows.Forms.CheckBox();
            this.ckTa1AckRequest = new System.Windows.Forms.CheckBox();
            this.btnAs2LogFolder = new System.Windows.Forms.Button();
            this.btnAs2FailedFolder = new System.Windows.Forms.Button();
            this.btnAs2SentFolder = new System.Windows.Forms.Button();
            this.btn997Folder = new System.Windows.Forms.Button();
            this.btnDoneFolder = new System.Windows.Forms.Button();
            this.btnRejectedFolder = new System.Windows.Forms.Button();
            this.btnOutboundFolder = new System.Windows.Forms.Button();
            this.btnSefFolder = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.txtCompanyRejectedFolder = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtCompanyDoneFolder = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtCompany997Folder = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtCompanySefFolder = new System.Windows.Forms.TextBox();
            this.btnInboundFolder = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFailedFolder = new System.Windows.Forms.TextBox();
            this.ckSynchronous = new System.Windows.Forms.CheckBox();
            this.ckReceiveSignedMDN = new System.Windows.Forms.CheckBox();
            this.ckReceiveMDN = new System.Windows.Forms.CheckBox();
            this.ckBase64Encoding = new System.Windows.Forms.CheckBox();
            this.ckEnableCompression = new System.Windows.Forms.CheckBox();
            this.ckEnableAssurance = new System.Windows.Forms.CheckBox();
            this.ckEnableEncryption = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtReceiptDeliveryOption = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDispositionNotificationTo = new System.Windows.Forms.TextBox();
            this.txtTargetURL = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSentFolder = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAS2_Folder = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCompanyInboundFolder = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCompanyOutboundFolder = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtEDI_CompanyId = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAS2_CompanyId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.EDI_CompanyIdQlfr = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cmbCertificateSubjName
            // 
            this.cmbCertificateSubjName.Location = new System.Drawing.Point(167, 425);
            this.cmbCertificateSubjName.Name = "cmbCertificateSubjName";
            this.cmbCertificateSubjName.Size = new System.Drawing.Size(512, 21);
            this.cmbCertificateSubjName.TabIndex = 161;
            // 
            // cmbCryptographicServiceProvider
            // 
            this.cmbCryptographicServiceProvider.Location = new System.Drawing.Point(167, 344);
            this.cmbCryptographicServiceProvider.Name = "cmbCryptographicServiceProvider";
            this.cmbCryptographicServiceProvider.Size = new System.Drawing.Size(512, 21);
            this.cmbCryptographicServiceProvider.TabIndex = 156;
            this.cmbCryptographicServiceProvider.Text = "Microsoft Strong Cryptographic Provider";
            // 
            // cmbCertificateStoreName
            // 
            this.cmbCertificateStoreName.Location = new System.Drawing.Point(480, 396);
            this.cmbCertificateStoreName.Name = "cmbCertificateStoreName";
            this.cmbCertificateStoreName.Size = new System.Drawing.Size(199, 21);
            this.cmbCertificateStoreName.TabIndex = 160;
            this.cmbCertificateStoreName.Text = "My";
            // 
            // cmbCertificateStoreLocation
            // 
            this.cmbCertificateStoreLocation.Location = new System.Drawing.Point(167, 396);
            this.cmbCertificateStoreLocation.Name = "cmbCertificateStoreLocation";
            this.cmbCertificateStoreLocation.Size = new System.Drawing.Size(218, 21);
            this.cmbCertificateStoreLocation.TabIndex = 159;
            this.cmbCertificateStoreLocation.Text = "CurrentUser";
            // 
            // cmbSigningHash
            // 
            this.cmbSigningHash.Location = new System.Drawing.Point(480, 369);
            this.cmbSigningHash.Name = "cmbSigningHash";
            this.cmbSigningHash.Size = new System.Drawing.Size(199, 21);
            this.cmbSigningHash.TabIndex = 158;
            this.cmbSigningHash.Text = "SHA-1";
            // 
            // cmbEncryptionAlgorithm
            // 
            this.cmbEncryptionAlgorithm.Location = new System.Drawing.Point(167, 369);
            this.cmbEncryptionAlgorithm.Name = "cmbEncryptionAlgorithm";
            this.cmbEncryptionAlgorithm.Size = new System.Drawing.Size(218, 21);
            this.cmbEncryptionAlgorithm.TabIndex = 157;
            this.cmbEncryptionAlgorithm.Text = "3DES";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(404, 457);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(74, 13);
            this.label23.TabIndex = 198;
            this.label23.Text = "Content Type:";
            // 
            // txtContentType
            // 
            this.txtContentType.Location = new System.Drawing.Point(480, 454);
            this.txtContentType.Name = "txtContentType";
            this.txtContentType.Size = new System.Drawing.Size(199, 20);
            this.txtContentType.TabIndex = 163;
            this.txtContentType.Text = "Application/EDI-X12";
            // 
            // ck997AckRequest
            // 
            this.ck997AckRequest.AutoSize = true;
            this.ck997AckRequest.Location = new System.Drawing.Point(304, 229);
            this.ck997AckRequest.Name = "ck997AckRequest";
            this.ck997AckRequest.Size = new System.Drawing.Size(109, 17);
            this.ck997AckRequest.TabIndex = 149;
            this.ck997AckRequest.Text = "997 Ack Request";
            this.ck997AckRequest.UseVisualStyleBackColor = true;
            // 
            // ckTa1AckRequest
            // 
            this.ckTa1AckRequest.AutoSize = true;
            this.ckTa1AckRequest.Location = new System.Drawing.Point(169, 229);
            this.ckTa1AckRequest.Name = "ckTa1AckRequest";
            this.ckTa1AckRequest.Size = new System.Drawing.Size(111, 17);
            this.ckTa1AckRequest.TabIndex = 148;
            this.ckTa1AckRequest.Text = "TA1 Ack Request";
            this.ckTa1AckRequest.UseVisualStyleBackColor = true;
            // 
            // btnAs2LogFolder
            // 
            this.btnAs2LogFolder.Location = new System.Drawing.Point(685, 313);
            this.btnAs2LogFolder.Name = "btnAs2LogFolder";
            this.btnAs2LogFolder.Size = new System.Drawing.Size(37, 20);
            this.btnAs2LogFolder.TabIndex = 155;
            this.btnAs2LogFolder.Text = "...";
            this.btnAs2LogFolder.UseVisualStyleBackColor = true;
            this.btnAs2LogFolder.Click += new System.EventHandler(this.btnAs2LogFolder_Click_1);
            // 
            // btnAs2FailedFolder
            // 
            this.btnAs2FailedFolder.Location = new System.Drawing.Point(685, 287);
            this.btnAs2FailedFolder.Name = "btnAs2FailedFolder";
            this.btnAs2FailedFolder.Size = new System.Drawing.Size(37, 20);
            this.btnAs2FailedFolder.TabIndex = 153;
            this.btnAs2FailedFolder.Text = "...";
            this.btnAs2FailedFolder.UseVisualStyleBackColor = true;
            this.btnAs2FailedFolder.Click += new System.EventHandler(this.btnAs2FailedFolder_Click_1);
            // 
            // btnAs2SentFolder
            // 
            this.btnAs2SentFolder.Location = new System.Drawing.Point(685, 261);
            this.btnAs2SentFolder.Name = "btnAs2SentFolder";
            this.btnAs2SentFolder.Size = new System.Drawing.Size(37, 20);
            this.btnAs2SentFolder.TabIndex = 151;
            this.btnAs2SentFolder.Text = "...";
            this.btnAs2SentFolder.UseVisualStyleBackColor = true;
            this.btnAs2SentFolder.Click += new System.EventHandler(this.btnAs2SentFolder_Click_1);
            // 
            // btn997Folder
            // 
            this.btn997Folder.Location = new System.Drawing.Point(685, 203);
            this.btn997Folder.Name = "btn997Folder";
            this.btn997Folder.Size = new System.Drawing.Size(37, 20);
            this.btn997Folder.TabIndex = 147;
            this.btn997Folder.Text = "...";
            this.btn997Folder.UseVisualStyleBackColor = true;
            this.btn997Folder.Click += new System.EventHandler(this.btn997Folder_Click_1);
            // 
            // btnDoneFolder
            // 
            this.btnDoneFolder.Location = new System.Drawing.Point(685, 177);
            this.btnDoneFolder.Name = "btnDoneFolder";
            this.btnDoneFolder.Size = new System.Drawing.Size(37, 20);
            this.btnDoneFolder.TabIndex = 145;
            this.btnDoneFolder.Text = "...";
            this.btnDoneFolder.UseVisualStyleBackColor = true;
            this.btnDoneFolder.Click += new System.EventHandler(this.btnDoneFolder_Click_1);
            // 
            // btnRejectedFolder
            // 
            this.btnRejectedFolder.Location = new System.Drawing.Point(685, 151);
            this.btnRejectedFolder.Name = "btnRejectedFolder";
            this.btnRejectedFolder.Size = new System.Drawing.Size(37, 20);
            this.btnRejectedFolder.TabIndex = 143;
            this.btnRejectedFolder.Text = "...";
            this.btnRejectedFolder.UseVisualStyleBackColor = true;
            this.btnRejectedFolder.Click += new System.EventHandler(this.btnRejectedFolder_Click_1);
            // 
            // btnOutboundFolder
            // 
            this.btnOutboundFolder.Location = new System.Drawing.Point(685, 125);
            this.btnOutboundFolder.Name = "btnOutboundFolder";
            this.btnOutboundFolder.Size = new System.Drawing.Size(37, 20);
            this.btnOutboundFolder.TabIndex = 141;
            this.btnOutboundFolder.Text = "...";
            this.btnOutboundFolder.UseVisualStyleBackColor = true;
            this.btnOutboundFolder.Click += new System.EventHandler(this.btnOutboundFolder_Click_1);
            // 
            // btnSefFolder
            // 
            this.btnSefFolder.Location = new System.Drawing.Point(685, 73);
            this.btnSefFolder.Name = "btnSefFolder";
            this.btnSefFolder.Size = new System.Drawing.Size(37, 20);
            this.btnSefFolder.TabIndex = 137;
            this.btnSefFolder.Text = "...";
            this.btnSefFolder.UseVisualStyleBackColor = true;
            this.btnSefFolder.Click += new System.EventHandler(this.btnSefFolder_Click_1);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(80, 154);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(85, 13);
            this.label20.TabIndex = 197;
            this.label20.Text = "Rejected Folder:";
            // 
            // txtCompanyRejectedFolder
            // 
            this.txtCompanyRejectedFolder.Location = new System.Drawing.Point(167, 151);
            this.txtCompanyRejectedFolder.Name = "txtCompanyRejectedFolder";
            this.txtCompanyRejectedFolder.Size = new System.Drawing.Size(512, 20);
            this.txtCompanyRejectedFolder.TabIndex = 142;
            this.txtCompanyRejectedFolder.Text = "EDI_REJECTED\\";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(97, 180);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 13);
            this.label21.TabIndex = 196;
            this.label21.Text = "Done Folder:";
            // 
            // txtCompanyDoneFolder
            // 
            this.txtCompanyDoneFolder.Location = new System.Drawing.Point(167, 177);
            this.txtCompanyDoneFolder.Name = "txtCompanyDoneFolder";
            this.txtCompanyDoneFolder.Size = new System.Drawing.Size(512, 20);
            this.txtCompanyDoneFolder.TabIndex = 144;
            this.txtCompanyDoneFolder.Text = "EDI_DONE\\";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(105, 206);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 13);
            this.label22.TabIndex = 195;
            this.label22.Text = "997 Folder:";
            // 
            // txtCompany997Folder
            // 
            this.txtCompany997Folder.Location = new System.Drawing.Point(167, 203);
            this.txtCompany997Folder.Name = "txtCompany997Folder";
            this.txtCompany997Folder.Size = new System.Drawing.Size(512, 20);
            this.txtCompany997Folder.TabIndex = 146;
            this.txtCompany997Folder.Text = "EDI_997\\";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(103, 76);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 13);
            this.label19.TabIndex = 194;
            this.label19.Text = "SEF Folder:";
            // 
            // txtCompanySefFolder
            // 
            this.txtCompanySefFolder.Location = new System.Drawing.Point(167, 73);
            this.txtCompanySefFolder.Name = "txtCompanySefFolder";
            this.txtCompanySefFolder.Size = new System.Drawing.Size(512, 20);
            this.txtCompanySefFolder.TabIndex = 136;
            this.txtCompanySefFolder.Text = "SEF\\";
            // 
            // btnInboundFolder
            // 
            this.btnInboundFolder.Location = new System.Drawing.Point(685, 99);
            this.btnInboundFolder.Name = "btnInboundFolder";
            this.btnInboundFolder.Size = new System.Drawing.Size(37, 20);
            this.btnInboundFolder.TabIndex = 139;
            this.btnInboundFolder.Text = "...";
            this.btnInboundFolder.UseVisualStyleBackColor = true;
            this.btnInboundFolder.Click += new System.EventHandler(this.btnInboundFolder_Click_1);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(221, 612);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(123, 35);
            this.btnDelete.TabIndex = 178;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 399);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 13);
            this.label4.TabIndex = 193;
            this.label4.Text = "Certificate Store Location:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(394, 399);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 13);
            this.label16.TabIndex = 192;
            this.label16.Text = "Certificate Store:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 347);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(156, 13);
            this.label17.TabIndex = 191;
            this.label17.Text = "Cryptographic Service Provider:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(38, 428);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(127, 13);
            this.label18.TabIndex = 190;
            this.label18.Text = "Certificate Subject Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(67, 290);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 189;
            this.label3.Text = "Failed Send Folder:";
            // 
            // txtFailedFolder
            // 
            this.txtFailedFolder.Location = new System.Drawing.Point(167, 287);
            this.txtFailedFolder.Name = "txtFailedFolder";
            this.txtFailedFolder.Size = new System.Drawing.Size(512, 20);
            this.txtFailedFolder.TabIndex = 152;
            this.txtFailedFolder.Text = "EDI_SENT_FAILED\\";
            // 
            // ckSynchronous
            // 
            this.ckSynchronous.AutoSize = true;
            this.ckSynchronous.Location = new System.Drawing.Point(441, 565);
            this.ckSynchronous.Name = "ckSynchronous";
            this.ckSynchronous.Size = new System.Drawing.Size(164, 17);
            this.ckSynchronous.TabIndex = 171;
            this.ckSynchronous.Text = "Receive MDN synchronously";
            this.ckSynchronous.UseVisualStyleBackColor = true;
            // 
            // ckReceiveSignedMDN
            // 
            this.ckReceiveSignedMDN.AutoSize = true;
            this.ckReceiveSignedMDN.Location = new System.Drawing.Point(549, 542);
            this.ckReceiveSignedMDN.Name = "ckReceiveSignedMDN";
            this.ckReceiveSignedMDN.Size = new System.Drawing.Size(130, 17);
            this.ckReceiveSignedMDN.TabIndex = 173;
            this.ckReceiveSignedMDN.Text = "Receive Signed MDN";
            this.ckReceiveSignedMDN.UseVisualStyleBackColor = true;
            // 
            // ckReceiveMDN
            // 
            this.ckReceiveMDN.AutoSize = true;
            this.ckReceiveMDN.Location = new System.Drawing.Point(441, 542);
            this.ckReceiveMDN.Name = "ckReceiveMDN";
            this.ckReceiveMDN.Size = new System.Drawing.Size(94, 17);
            this.ckReceiveMDN.TabIndex = 170;
            this.ckReceiveMDN.Text = "Receive MDN";
            this.ckReceiveMDN.UseVisualStyleBackColor = true;
            // 
            // ckBase64Encoding
            // 
            this.ckBase64Encoding.AutoSize = true;
            this.ckBase64Encoding.Location = new System.Drawing.Point(300, 542);
            this.ckBase64Encoding.Name = "ckBase64Encoding";
            this.ckBase64Encoding.Size = new System.Drawing.Size(113, 17);
            this.ckBase64Encoding.TabIndex = 168;
            this.ckBase64Encoding.Text = "Base 64 Encoding";
            this.ckBase64Encoding.UseVisualStyleBackColor = true;
            // 
            // ckEnableCompression
            // 
            this.ckEnableCompression.AutoSize = true;
            this.ckEnableCompression.Location = new System.Drawing.Point(300, 565);
            this.ckEnableCompression.Name = "ckEnableCompression";
            this.ckEnableCompression.Size = new System.Drawing.Size(122, 17);
            this.ckEnableCompression.TabIndex = 169;
            this.ckEnableCompression.Text = "Enable Compression";
            this.ckEnableCompression.UseVisualStyleBackColor = true;
            // 
            // ckEnableAssurance
            // 
            this.ckEnableAssurance.AutoSize = true;
            this.ckEnableAssurance.Location = new System.Drawing.Point(167, 565);
            this.ckEnableAssurance.Name = "ckEnableAssurance";
            this.ckEnableAssurance.Size = new System.Drawing.Size(112, 17);
            this.ckEnableAssurance.TabIndex = 167;
            this.ckEnableAssurance.Text = "Enable Assurance";
            this.ckEnableAssurance.UseVisualStyleBackColor = true;
            // 
            // ckEnableEncryption
            // 
            this.ckEnableEncryption.AutoSize = true;
            this.ckEnableEncryption.Location = new System.Drawing.Point(167, 542);
            this.ckEnableEncryption.Name = "ckEnableEncryption";
            this.ckEnableEncryption.Size = new System.Drawing.Size(112, 17);
            this.ckEnableEncryption.TabIndex = 166;
            this.ckEnableEncryption.Text = "Enable Encryption";
            this.ckEnableEncryption.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(43, 509);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label15.Size = new System.Drawing.Size(122, 13);
            this.label15.TabIndex = 188;
            this.label15.Text = "Receipt Delivery Option:";
            // 
            // txtReceiptDeliveryOption
            // 
            this.txtReceiptDeliveryOption.Location = new System.Drawing.Point(167, 506);
            this.txtReceiptDeliveryOption.Name = "txtReceiptDeliveryOption";
            this.txtReceiptDeliveryOption.Size = new System.Drawing.Size(512, 20);
            this.txtReceiptDeliveryOption.TabIndex = 165;
            this.txtReceiptDeliveryOption.Text = "http://domain.net:999/AsyMDN/";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(32, 457);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(133, 13);
            this.label13.TabIndex = 187;
            this.label13.Text = "Disposition Notification To:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(99, 483);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 13);
            this.label14.TabIndex = 186;
            this.label14.Text = "Target URL:";
            // 
            // txtDispositionNotificationTo
            // 
            this.txtDispositionNotificationTo.Location = new System.Drawing.Point(167, 454);
            this.txtDispositionNotificationTo.Name = "txtDispositionNotificationTo";
            this.txtDispositionNotificationTo.Size = new System.Drawing.Size(218, 20);
            this.txtDispositionNotificationTo.TabIndex = 162;
            this.txtDispositionNotificationTo.Text = "user@domain.net";
            // 
            // txtTargetURL
            // 
            this.txtTargetURL.Location = new System.Drawing.Point(167, 480);
            this.txtTargetURL.Name = "txtTargetURL";
            this.txtTargetURL.Size = new System.Drawing.Size(512, 20);
            this.txtTargetURL.TabIndex = 164;
            this.txtTargetURL.Text = "http://domain.net:5000/As2Server/As2Server.aspx/";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(406, 373);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 185;
            this.label10.Text = "Signing Hash:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(60, 373);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(106, 13);
            this.label12.TabIndex = 184;
            this.label12.Text = "Encryption Algorithm:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(78, 264);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 183;
            this.label7.Text = "AS2 Sent Folder:";
            // 
            // txtSentFolder
            // 
            this.txtSentFolder.Location = new System.Drawing.Point(167, 261);
            this.txtSentFolder.Name = "txtSentFolder";
            this.txtSentFolder.Size = new System.Drawing.Size(512, 20);
            this.txtSentFolder.TabIndex = 150;
            this.txtSentFolder.Text = "EDI_SENT\\";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(82, 316);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 182;
            this.label9.Text = "AS2 Log Folder:";
            // 
            // txtAS2_Folder
            // 
            this.txtAS2_Folder.Location = new System.Drawing.Point(167, 313);
            this.txtAS2_Folder.Name = "txtAS2_Folder";
            this.txtAS2_Folder.Size = new System.Drawing.Size(512, 20);
            this.txtAS2_Folder.TabIndex = 154;
            this.txtAS2_Folder.Text = "AS2_OUTPUT\\";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(397, 612);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(123, 35);
            this.btnCancel.TabIndex = 181;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click_1);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(232, 612);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(123, 35);
            this.btnSave.TabIndex = 180;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(84, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 179;
            this.label5.Text = "Inbound Folder:";
            // 
            // txtCompanyInboundFolder
            // 
            this.txtCompanyInboundFolder.Location = new System.Drawing.Point(167, 99);
            this.txtCompanyInboundFolder.Name = "txtCompanyInboundFolder";
            this.txtCompanyInboundFolder.Size = new System.Drawing.Size(512, 20);
            this.txtCompanyInboundFolder.TabIndex = 138;
            this.txtCompanyInboundFolder.Text = "EDI_INBOUND\\TradingPartnernName\\";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(76, 128);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 177;
            this.label6.Text = "Outbound Folder:";
            // 
            // txtCompanyOutboundFolder
            // 
            this.txtCompanyOutboundFolder.Location = new System.Drawing.Point(167, 125);
            this.txtCompanyOutboundFolder.Name = "txtCompanyOutboundFolder";
            this.txtCompanyOutboundFolder.Size = new System.Drawing.Size(512, 20);
            this.txtCompanyOutboundFolder.TabIndex = 140;
            this.txtCompanyOutboundFolder.Text = "EDI_OUTBOUND\\";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(254, 42);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 13);
            this.label11.TabIndex = 176;
            this.label11.Text = "EDI Company ID:";
            // 
            // txtEDI_CompanyId
            // 
            this.txtEDI_CompanyId.Location = new System.Drawing.Point(345, 39);
            this.txtEDI_CompanyId.Name = "txtEDI_CompanyId";
            this.txtEDI_CompanyId.Size = new System.Drawing.Size(105, 20);
            this.txtEDI_CompanyId.TabIndex = 134;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(481, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 13);
            this.label8.TabIndex = 175;
            this.label8.Text = "AS2 Company ID:";
            // 
            // txtAS2_CompanyId
            // 
            this.txtAS2_CompanyId.Location = new System.Drawing.Point(574, 39);
            this.txtAS2_CompanyId.Name = "txtAS2_CompanyId";
            this.txtAS2_CompanyId.Size = new System.Drawing.Size(105, 20);
            this.txtAS2_CompanyId.TabIndex = 135;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 174;
            this.label2.Text = "EDI Company ID Qlfr:";
            // 
            // EDI_CompanyIdQlfr
            // 
            this.EDI_CompanyIdQlfr.Location = new System.Drawing.Point(167, 39);
            this.EDI_CompanyIdQlfr.Name = "EDI_CompanyIdQlfr";
            this.EDI_CompanyIdQlfr.Size = new System.Drawing.Size(52, 20);
            this.EDI_CompanyIdQlfr.TabIndex = 133;
            this.EDI_CompanyIdQlfr.Text = "ZZ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 172;
            this.label1.Text = "Company Name:";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Location = new System.Drawing.Point(167, 13);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(512, 20);
            this.txtCompanyName.TabIndex = 132;
            // 
            // frmTradingPartnerProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 650);
            this.Controls.Add(this.cmbCertificateSubjName);
            this.Controls.Add(this.cmbCryptographicServiceProvider);
            this.Controls.Add(this.cmbCertificateStoreName);
            this.Controls.Add(this.cmbCertificateStoreLocation);
            this.Controls.Add(this.cmbSigningHash);
            this.Controls.Add(this.cmbEncryptionAlgorithm);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.txtContentType);
            this.Controls.Add(this.ck997AckRequest);
            this.Controls.Add(this.ckTa1AckRequest);
            this.Controls.Add(this.btnAs2LogFolder);
            this.Controls.Add(this.btnAs2FailedFolder);
            this.Controls.Add(this.btnAs2SentFolder);
            this.Controls.Add(this.btn997Folder);
            this.Controls.Add(this.btnDoneFolder);
            this.Controls.Add(this.btnRejectedFolder);
            this.Controls.Add(this.btnOutboundFolder);
            this.Controls.Add(this.btnSefFolder);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtCompanyRejectedFolder);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtCompanyDoneFolder);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.txtCompany997Folder);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtCompanySefFolder);
            this.Controls.Add(this.btnInboundFolder);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtFailedFolder);
            this.Controls.Add(this.ckSynchronous);
            this.Controls.Add(this.ckReceiveSignedMDN);
            this.Controls.Add(this.ckReceiveMDN);
            this.Controls.Add(this.ckBase64Encoding);
            this.Controls.Add(this.ckEnableCompression);
            this.Controls.Add(this.ckEnableAssurance);
            this.Controls.Add(this.ckEnableEncryption);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtReceiptDeliveryOption);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtDispositionNotificationTo);
            this.Controls.Add(this.txtTargetURL);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtSentFolder);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtAS2_Folder);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCompanyInboundFolder);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtCompanyOutboundFolder);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtEDI_CompanyId);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtAS2_CompanyId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.EDI_CompanyIdQlfr);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCompanyName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmTradingPartnerProfile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmTradingPartnerProfile";
            this.Load += new System.EventHandler(this.frmTradingPartnerProfile_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ComboBox cmbCertificateSubjName;
        internal System.Windows.Forms.ComboBox cmbCryptographicServiceProvider;
        internal System.Windows.Forms.ComboBox cmbCertificateStoreName;
        internal System.Windows.Forms.ComboBox cmbCertificateStoreLocation;
        internal System.Windows.Forms.ComboBox cmbSigningHash;
        internal System.Windows.Forms.ComboBox cmbEncryptionAlgorithm;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtContentType;
        private System.Windows.Forms.CheckBox ck997AckRequest;
        private System.Windows.Forms.CheckBox ckTa1AckRequest;
        private System.Windows.Forms.Button btnAs2LogFolder;
        private System.Windows.Forms.Button btnAs2FailedFolder;
        private System.Windows.Forms.Button btnAs2SentFolder;
        private System.Windows.Forms.Button btn997Folder;
        private System.Windows.Forms.Button btnDoneFolder;
        private System.Windows.Forms.Button btnRejectedFolder;
        private System.Windows.Forms.Button btnOutboundFolder;
        private System.Windows.Forms.Button btnSefFolder;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtCompanyRejectedFolder;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtCompanyDoneFolder;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtCompany997Folder;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtCompanySefFolder;
        private System.Windows.Forms.Button btnInboundFolder;
        public System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFailedFolder;
        private System.Windows.Forms.CheckBox ckSynchronous;
        private System.Windows.Forms.CheckBox ckReceiveSignedMDN;
        private System.Windows.Forms.CheckBox ckReceiveMDN;
        private System.Windows.Forms.CheckBox ckBase64Encoding;
        private System.Windows.Forms.CheckBox ckEnableCompression;
        private System.Windows.Forms.CheckBox ckEnableAssurance;
        private System.Windows.Forms.CheckBox ckEnableEncryption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtReceiptDeliveryOption;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtDispositionNotificationTo;
        private System.Windows.Forms.TextBox txtTargetURL;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSentFolder;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtAS2_Folder;
        private System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCompanyInboundFolder;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCompanyOutboundFolder;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtEDI_CompanyId;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAS2_CompanyId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox EDI_CompanyIdQlfr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCompanyName;
    }
}