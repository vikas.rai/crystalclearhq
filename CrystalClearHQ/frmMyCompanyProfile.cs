﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Edidev.FrameworkEDIx64;


namespace CrystalClearHQ
{
    public partial class frmMyCompanyProfile : Form
    {
        public frmMyCompanyProfile()
        {
            InitializeComponent();
        }

        private SqlConnection oConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ClearEdi"].ConnectionString);

        private void frmMyCompanySetup_Load(object sender, EventArgs e)
        {
            string sMyCompanyName = ConfigurationManager.AppSettings["MyCompanyName"];
            string sEdiSentFailed = ConfigurationManager.AppSettings["EDI_SENT_FAILED"];
            this.Text = sMyCompanyName + " Configuration";

            oConnection.Open();

            string sSql = "select * from [MyCompany] ";
            SqlDataAdapter oDa = new SqlDataAdapter(sSql, oConnection);
            DataSet oDs = new DataSet("dsMyCompany");
            oDa.Fill(oDs, "dsMyCompany");

            if (oDs.Tables["dsMyCompany"].Rows.Count > 0)
            {
                foreach (DataRow oMyCompanyRow in oDs.Tables["dsMyCompany"].Rows)
                {
                    txtCompanyName.Text = oMyCompanyRow["CompanyName"].ToString();
                    txtEdiCompanyIdQlfr.Text = oMyCompanyRow["EDI_CompanyIdQlfr"].ToString();
                    txtEdiCompanyId.Text = oMyCompanyRow["EDI_CompanyId"].ToString();
                    txtControlNumber.Text = oMyCompanyRow["ControlNumber"].ToString();
                    txtAckRequested.Text = oMyCompanyRow["AcknowledgmentRequested"].ToString();
                    txtUsageIndicator.Text = oMyCompanyRow["UsageIndicator"].ToString();
                    txtComponentSeparator.Text = oMyCompanyRow["ComponentElementSeparator"].ToString();
                    txtSegmentTerminator.Text = oMyCompanyRow["SegmentTerminator"].ToString();
                    txtElementTerminator.Text = oMyCompanyRow["ElementTerminator"].ToString();
                    txtRepeatSeparator.Text = oMyCompanyRow["RepeatSeparator"].ToString();
                    txtReleaseIndicator.Text = oMyCompanyRow["ReleaseIndicator"].ToString();

                    txtAs2CompanyID.Text = oMyCompanyRow["AS2_CompanyId"].ToString();
                    txtAs2MessageID.Text = oMyCompanyRow["AS2_MessageID"].ToString();
                    txtCompanyDomain.Text = oMyCompanyRow["CompanyDomain"].ToString();
                    cmbCertificateSubjName.Text = oMyCompanyRow["CertificateSubjName"].ToString();
                    cmbCryptographicServiceProvider.Text = oMyCompanyRow["CryptographicServiceProvider"].ToString();
                    cmbCertificateStoreLocation.Text = oMyCompanyRow["CertificateStoreLocation"].ToString();
                    cmbCertificateStoreName.Text = oMyCompanyRow["CertificateStoreName"].ToString();

                }

            }
            else
            {
                // add a record with default values if no record exists
                sSql = @"INSERT INTO [MyCompany] 
                            (CompanyName, EDI_CompanyIdQlfr, EDI_CompanyId, ControlNumber, AcknowledgmentRequested, UsageIndicator, ComponentElementSeparator, 
                            SegmentTerminator, ElementTerminator, RepeatSeparator, ReleaseIndicator,
                            AS2_CompanyID, AS2_MessageID, CompanyDomain, CertificateSubjName, CryptographicServiceProvider, CertificateStoreLocation, CertificateStoreName)
                        values 
                            (@CompanyName, @EDI_CompanyIdQlfr, @EDI_CompanyId, @ControlNumber, @AcknowledgmentRequested, @UsageIndicator, @ComponentElementSeparator,  
                            @SegmentTerminator, @ElementTerminator, @RepeatSeparator, @ReleaseIndicator,
                            @AS2_CompanyID, @AS2_MessageID, @CompanyDomain, @CertificateSubjName, @CryptographicServiceProvider, @CertificateStoreLocation, @CertificateStoreName)";

                oDa.InsertCommand = new SqlCommand(sSql, oConnection);
                oDa.InsertCommand.Parameters.AddWithValue("@CompanyName", "MyDemoTestCompany");
                oDa.InsertCommand.Parameters.AddWithValue("@EDI_CompanyIdQlfr", "ZZ");
                oDa.InsertCommand.Parameters.AddWithValue("@EDI_CompanyId", "DEMOTESTCO");
                oDa.InsertCommand.Parameters.AddWithValue("@ControlNumber", "70");
                oDa.InsertCommand.Parameters.AddWithValue("@AcknowledgmentRequested", "0");
                oDa.InsertCommand.Parameters.AddWithValue("@UsageIndicator", "T");
                oDa.InsertCommand.Parameters.AddWithValue("@ComponentElementSeparator", ">");
                oDa.InsertCommand.Parameters.AddWithValue("@SegmentTerminator", "~{13:10}");
                oDa.InsertCommand.Parameters.AddWithValue("@ElementTerminator", "*");
                oDa.InsertCommand.Parameters.AddWithValue("@RepeatSeparator", "^");
                oDa.InsertCommand.Parameters.AddWithValue("@ReleaseIndicator", "");
                oDa.InsertCommand.Parameters.AddWithValue("@AS2_CompanyID", "DEMOTESTCO");
                oDa.InsertCommand.Parameters.AddWithValue("@AS2_MessageID", "10001");
                oDa.InsertCommand.Parameters.AddWithValue("@CompanyDomain", "demotestco.com");
                oDa.InsertCommand.Parameters.AddWithValue("@CertificateSubjName", "DemoTestCo_cert");
                oDa.InsertCommand.Parameters.AddWithValue("@CryptographicServiceProvider", "Microsoft Enhanced RSA and AES Cryptographic Provider");
                oDa.InsertCommand.Parameters.AddWithValue("@CertificateStoreLocation", "CurrentUser");
                oDa.InsertCommand.Parameters.AddWithValue("@CertificateStoreName", "My");
                oDa.InsertCommand.ExecuteScalar();

                sSql = "select * from [MyCompany] ";
                oDa = new SqlDataAdapter(sSql, oConnection);
                oDs = new DataSet("dsMyCompany");
                oDa.Fill(oDs, "dsMyCompany");

                foreach (DataRow oMyCompanyRow in oDs.Tables["dsMyCompany"].Rows)
                {
                    txtCompanyName.Text = oMyCompanyRow["CompanyName"].ToString();
                    txtEdiCompanyIdQlfr.Text = oMyCompanyRow["EDI_CompanyIdQlfr"].ToString();
                    txtEdiCompanyId.Text = oMyCompanyRow["EDI_CompanyId"].ToString();
                    txtControlNumber.Text = oMyCompanyRow["ControlNumber"].ToString();
                    txtAckRequested.Text = oMyCompanyRow["AcknowledgmentRequested"].ToString();
                    txtUsageIndicator.Text = oMyCompanyRow["UsageIndicator"].ToString();
                    txtComponentSeparator.Text = oMyCompanyRow["ComponentElementSeparator"].ToString();
                    txtSegmentTerminator.Text = oMyCompanyRow["SegmentTerminator"].ToString();
                    txtElementTerminator.Text = oMyCompanyRow["ElementTerminator"].ToString();
                    txtRepeatSeparator.Text = oMyCompanyRow["RepeatSeparator"].ToString();
                    txtReleaseIndicator.Text = oMyCompanyRow["ReleaseIndicator"].ToString();

                    txtAs2CompanyID.Text = oMyCompanyRow["AS2_CompanyId"].ToString();
                    txtAs2MessageID.Text = oMyCompanyRow["AS2_MessageID"].ToString();
                    txtCompanyDomain.Text = oMyCompanyRow["CompanyDomain"].ToString();
                    cmbCertificateSubjName.Text = oMyCompanyRow["CertificateSubjName"].ToString();
                    cmbCryptographicServiceProvider.Text = oMyCompanyRow["CryptographicServiceProvider"].ToString();
                    cmbCertificateStoreLocation.Text = oMyCompanyRow["CertificateStoreLocation"].ToString();
                    cmbCertificateStoreName.Text = oMyCompanyRow["CertificateStoreName"].ToString();
                }
            }

            oConnection.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            oConnection.Open();

            SqlDataAdapter oDa = new SqlDataAdapter();

            string sSql = @"UPDATE [MyCompany] SET CompanyName = @CompanyName, EDI_CompanyIdQlfr = @EDI_CompanyIdQlfr, 
                        EDI_CompanyId = @EDI_CompanyId, ControlNumber = @ControlNumber, AcknowledgmentRequested = @AcknowledgmentRequested, 
                        UsageIndicator = @UsageIndicator, ComponentElementSeparator = @ComponentElementSeparator,
                        AS2_CompanyId = @AS2_CompanyId, AS2_MessageID = @AS2_MessageID, CompanyDomain = @CompanyDomain,
                        CertificateSubjName = @CertificateSubjName, CryptographicServiceProvider = @CryptographicServiceProvider, 
                        CertificateStoreLocation = @CertificateStoreLocation, CertificateStoreName = @CertificateStoreName ";

            oDa.UpdateCommand = new SqlCommand(sSql, oConnection);
            oDa.UpdateCommand.Parameters.AddWithValue("@CompanyName", txtCompanyName.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@EDI_CompanyIdQlfr", txtEdiCompanyIdQlfr.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@EDI_CompanyId", txtEdiCompanyId.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@ControlNumber", txtControlNumber.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@AcknowledgmentRequested", txtAckRequested.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@UsageIndicator", txtUsageIndicator.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@ComponentElementSeparator", txtComponentSeparator.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@SegmentTerminator", txtSegmentTerminator.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@ElementTerminator", txtElementTerminator.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@RepeatSeparator", txtRepeatSeparator.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@ReleaseIndicator", txtReleaseIndicator.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@AS2_CompanyId", txtAs2CompanyID.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@AS2_MessageID", txtAs2MessageID.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@CompanyDomain", txtCompanyDomain.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@CertificateSubjName", cmbCertificateSubjName.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@CryptographicServiceProvider", cmbCryptographicServiceProvider.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@CertificateStoreLocation", cmbCertificateStoreLocation.Text);
            oDa.UpdateCommand.Parameters.AddWithValue("@CertificateStoreName", cmbCertificateStoreName.Text);
            oDa.UpdateCommand.ExecuteNonQuery();

            oConnection.Close();

            MessageBox.Show("Save Successful");

            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();

        }

        private void cmbCryptographicServiceProvider_DropDown(object sender, EventArgs e)
        {
            if (cmbCryptographicServiceProvider.Items.Count == 0)
            {
                ediDocument oEdiDoc = new ediDocument();
                ediSecurities oSecurities = oEdiDoc.GetSecurities();
                ediSecurityServiceProviders oServProvs = oSecurities.GetServiceProviders();
                ediSecurityServiceProvider oServProv = oServProvs.First();

                for (int i = 1; i <= oServProvs.Count; i++)
                {
                    cmbCryptographicServiceProvider.Items.Add(oServProv.Name);
                    ediSecurityServiceProvider.Set(ref oServProv, oServProvs.Next());
                }

                if (cmbCryptographicServiceProvider.Text.Trim().Length == 0)
                {
                    cmbCryptographicServiceProvider.Text = "Microsoft Strong Cryptographic Provider";
                }

                if (oServProv != null)
                    oServProv.Dispose();

                if (oServProvs != null)
                    oServProvs.Dispose();

                if (oSecurities != null)
                    oSecurities.Dispose();

                if (oEdiDoc != null)
                    oEdiDoc.Dispose();

            }
        }

        private void cmbCertificateStoreLocation_DropDown(object sender, EventArgs e)
        {
            if (cmbCertificateStoreLocation.Items.Count == 0)
            {
                ediDocument oEdiDoc = new ediDocument();
                ediSecurities oSecurities = oEdiDoc.GetSecurities();
                ediSecurityCertStoreLocations oCertLocs = oSecurities.GetCertificateStoreLocations();
                ediSecurityCertificateStores oCertStores = oCertLocs.GetFirstCertificateStores();

                while (oCertStores != null)
                {

                    cmbCertificateStoreLocation.Items.Add(oCertStores.Location);

                    oCertStores = oCertLocs.GetNextCertificateStores();
                }

                if (cmbCertificateStoreLocation.Text.Trim().Length == 0)
                {
                    cmbCertificateStoreLocation.Text = "CurrentUser";
                }

                if (oCertStores != null)
                    oCertStores.Dispose();

                if (oCertLocs != null)
                    oCertLocs.Dispose();

                if (oSecurities != null)
                    oSecurities.Dispose();

                if (oEdiDoc != null)
                    oEdiDoc.Dispose();

            }
        }

        private void cmbCertificateStoreName_DropDown(object sender, EventArgs e)
        {
            if (cmbCertificateStoreName.Items.Count == 0)
            {
                ediDocument oEdiDoc = new ediDocument();
                ediSecurities oSecurities = oEdiDoc.GetSecurities();
                ediSecurityCertStoreLocations oCertLocs = oSecurities.GetCertificateStoreLocations();
                ediSecurityCertificateStores oCertStores = oCertLocs.GetCertificateStores(cmbCertificateStoreLocation.Text);
                ediSecurityCertificateStore oCertStore = oCertStores.GetFirstCertificateStore();

                for (int i = 1; i <= oCertStores.Count; i++)
                {
                    cmbCertificateStoreName.Items.Add(oCertStore.StoreName);
                    ediSecurityCertificateStore.Set(ref oCertStore, oCertStores.GetNextCertificateStore());
                }

                if (cmbCertificateStoreName.Text.Trim().Length == 0)
                {
                    cmbCertificateStoreName.Text = "My";
                }

                if (oCertStore != null)
                    oCertStore.Dispose();

                if (oCertStores != null)
                    oCertStores.Dispose();

                if (oCertLocs != null)
                    oCertLocs.Dispose();

                if (oSecurities != null)
                    oSecurities.Dispose();

                if (oEdiDoc != null)
                    oEdiDoc.Dispose();
            }
        }

        private void cmbCertificateSubjName_DropDown(object sender, EventArgs e)
        {
            if (cmbCertificateSubjName.Items.Count == 0)
            {
                //List all the certificates in a certificate store

                ediDocument oEdiDoc = new ediDocument();
                ediSecurities oSecurities = oEdiDoc.GetSecurities();
                ediSecurityCertStoreLocations oCertLoc = oSecurities.GetCertificateStoreLocations();
                ediSecurityCertificateStores oCertStores = oCertLoc.GetCertificateStores(cmbCertificateStoreLocation.Text);
                ediSecurityCertificateStore oCertStore = oCertStores.GetCertificateStore(cmbCertificateStoreName.Text);

                cmbCertificateSubjName.Items.Clear();

                ediSecurityCertificate oCert = oCertStore.GetFirstCertificate();

                for (int i = 1; i <= oCertStore.Count; i++)
                {
                    cmbCertificateSubjName.Items.Add(oCert.SubjectName);
                    ediSecurityCertificate.Set(ref oCert, oCertStore.GetNextCertificate());
                }

                if (oCertStore != null)
                    oCertStore.Dispose();

                if (oCertStores != null)
                    oCertStores.Dispose();

                if (oCertLoc != null)
                    oCertLoc.Dispose();

                if (oSecurities != null)
                    oSecurities.Dispose();

                if (oEdiDoc != null)
                    oEdiDoc.Dispose();

            }
        }

        private void frmMyCompanyProfile_Load(object sender, EventArgs e)
        {

        }
    }
}
