﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO;



namespace CrystalClearHQ
{
    public partial class ClearSetup : UserControl
    {
        public ClearSetup()
        {
            InitializeComponent();
        }

        private void btnCreateFolders_Click(object sender, EventArgs e)
        {
           
            string sEdiInbound = ConfigurationManager.AppSettings["EDI_Inbound"];
            string sEdiOutbound = ConfigurationManager.AppSettings["EDI_Outbound"];
            string sEdiRejected = ConfigurationManager.AppSettings["EDI_Rejected"];
            string sEdiDone = ConfigurationManager.AppSettings["EDI_Done"];
            string sEdi997 = ConfigurationManager.AppSettings["EDI_997"];
            string sEdiSent = ConfigurationManager.AppSettings["EDI_Sent"];
            string sEdiSentFailed = ConfigurationManager.AppSettings["EDI_Sent_Failed"];
            string sAS2_Output = ConfigurationManager.AppSettings["AS2_Output"];

            string sPath = AppDomain.CurrentDomain.BaseDirectory;

            if (!Directory.Exists(sPath + sEdiInbound))
            {
                Directory.CreateDirectory(sPath + sEdiInbound);
                Directory.CreateDirectory(sPath + sEdiInbound + @"\TradingPartnerA\");
                Directory.CreateDirectory(sPath + sEdiInbound + @"\TradingPartnerB\");
            }

            if (!Directory.Exists(sPath + sEdiOutbound))
            {
                Directory.CreateDirectory(sPath + sEdiOutbound);
            }

            if (!Directory.Exists(sPath + sEdiRejected))
            {
                Directory.CreateDirectory(sPath + sEdiRejected);
            }

            if (!Directory.Exists(sPath + sEdiDone))
            {
                Directory.CreateDirectory(sPath + sEdiDone);
            }

            if (!Directory.Exists(sPath + sEdi997))
            {
                Directory.CreateDirectory(sPath + sEdi997);
            }

            if (!Directory.Exists(sPath + sEdiSent))
            {
                Directory.CreateDirectory(sPath + sEdiSent);
            }

            if (!Directory.Exists(sPath + sEdiSentFailed))
            {
                Directory.CreateDirectory(sPath + sEdiSentFailed);
            }

            if (!Directory.Exists(sPath + sAS2_Output))
            {
                Directory.CreateDirectory(sPath + sAS2_Output);
            }

            MessageBox.Show("Done");
        }

        private void btnMyCompanySetup_Click(object sender, EventArgs e)
        {
            frmMyCompanyProfile ofrmMyCompanyProfile = new frmMyCompanyProfile();

            ofrmMyCompanyProfile.Show();
        }

        private void btnTradingPartner_Click(object sender, EventArgs e)
        {
            frmTradingPartnersList ofrmTradingPartnersList = new frmTradingPartnersList();

            ofrmTradingPartnersList.Show();
        }
    }
    }

