﻿namespace CrystalClearHQ
{
    partial class ClearSetup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreateFolders = new System.Windows.Forms.Button();
            this.btnTradingPartner = new System.Windows.Forms.Button();
            this.btnMyCompanySetup = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCreateFolders
            // 
            this.btnCreateFolders.Location = new System.Drawing.Point(3, 66);
            this.btnCreateFolders.Name = "btnCreateFolders";
            this.btnCreateFolders.Size = new System.Drawing.Size(133, 38);
            this.btnCreateFolders.TabIndex = 53;
            this.btnCreateFolders.Text = "Create Default Folders";
            this.btnCreateFolders.UseVisualStyleBackColor = true;
            this.btnCreateFolders.Click += new System.EventHandler(this.btnCreateFolders_Click);
            // 
            // btnTradingPartner
            // 
            this.btnTradingPartner.Location = new System.Drawing.Point(3, 190);
            this.btnTradingPartner.Name = "btnTradingPartner";
            this.btnTradingPartner.Size = new System.Drawing.Size(133, 38);
            this.btnTradingPartner.TabIndex = 52;
            this.btnTradingPartner.Text = "Trading Partner Setup";
            this.btnTradingPartner.UseVisualStyleBackColor = true;
            this.btnTradingPartner.Click += new System.EventHandler(this.btnTradingPartner_Click);
            // 
            // btnMyCompanySetup
            // 
            this.btnMyCompanySetup.Location = new System.Drawing.Point(3, 129);
            this.btnMyCompanySetup.Name = "btnMyCompanySetup";
            this.btnMyCompanySetup.Size = new System.Drawing.Size(133, 38);
            this.btnMyCompanySetup.TabIndex = 51;
            this.btnMyCompanySetup.Text = "My Company Setup";
            this.btnMyCompanySetup.UseVisualStyleBackColor = true;
            this.btnMyCompanySetup.Click += new System.EventHandler(this.btnMyCompanySetup_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 39);
            this.label1.TabIndex = 54;
            this.label1.Text = "CLEAR SETUP\r\n";
            // 
            // ClearSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCreateFolders);
            this.Controls.Add(this.btnTradingPartner);
            this.Controls.Add(this.btnMyCompanySetup);
            this.Name = "ClearSetup";
            this.Size = new System.Drawing.Size(832, 540);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCreateFolders;
        private System.Windows.Forms.Button btnTradingPartner;
        private System.Windows.Forms.Button btnMyCompanySetup;
        private System.Windows.Forms.Label label1;
    }
}
