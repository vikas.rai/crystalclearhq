﻿using CrystalClearHQ.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrystalClearHQ
{
    public partial class Dashboard : Form
    {
        private bool isCollapsed;
        private bool isCollaps;
        public Dashboard()
        {
            InitializeComponent();
        }

        private void exixtToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {
                button2.Image = Resources.Collapse_Arrow_20px;
                panel1.Height += 10;
                if (panel1.Size == panel1.MaximumSize)
                {
                    timer1.Stop();
                    isCollapsed = false;
                }
            }
            else
            {
                button2.Image = Resources.Expand_Arrow_20px;
                panel1.Height -= 10;
                if (panel1.Size == panel1.MinimumSize)
                {
                    timer1.Stop();
                    isCollapsed = true;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Dashboard_Load(object sender, EventArgs e)
        {
            label2.Text = "Administration: " + Environment.MachineName;
            ToolTip toolTip1 = new ToolTip();
            toolTip1.ShowAlways = true;
            toolTip1.SetToolTip(label2, label2.Text );
        }

        private void button11_Click(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (isCollaps)
            {
                button11.Image = Resources.Collapse_Arrow_20px;
                panel3.Height += 10;
                if (panel3.Size == panel3.MaximumSize)
                {
                    timer2.Stop();
                    isCollaps = false;
                }
            }
            else
            {
                button11.Image = Resources.Expand_Arrow_20px;
                panel3.Height -= 10;
                if (panel3.Size == panel3.MinimumSize)
                {
                    timer2.Stop();
                    isCollaps = true;
                }
            }
        }

        private void clearSetup1_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            firstPage1.BringToFront();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            clearSetup1.BringToFront();
        }
    }
}
