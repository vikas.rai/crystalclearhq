﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrystalClearHQ
{
    public partial class Splashfrm : Form
    {
        public Splashfrm()
        {
            InitializeComponent();
        }

        private void Splashfrm_Load(object sender, EventArgs e)
        {
            timer1.Start();
            timer1.Interval = 100;
            label4.Visible = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.Minimum = 0;
            progressBar1.Maximum = 101;
            if (progressBar1.Value < progressBar1.Maximum)
            {
                
                progressBar1.Value ++;
            }
            if (progressBar1.Value == 20)
            {
                label4.Visible = true;
                label4.Text = " Loading..";
            }
            else if (progressBar1.Value == 40)
            {
                label4.Visible = true;
                label4.Text = " initializing Environment....";
            }
            else if (progressBar1.Value == 60)
            {
                label4.Visible = true;
                label4.Text = " initializing Environment......";
            }
            else if (progressBar1.Value == 80)
            {
                label4.Visible = true;
                label4.Text = " Validating Data Source";
            }
            else if (progressBar1.Value == 90)
            {
                label4.Visible = true;
                label4.Text = " Validating Data Source..";
                
            }
            else if (progressBar1.Value == 100)
            {
                label4.Visible = true;
                label4.Text = " Validating Data Source--- Good";
            }
            else if (progressBar1.Value == 101)
            {
                
                Dashboard obj = new Dashboard();
                obj.Show();
                this.Hide();
                timer1.Stop();
            }
        }
    }
}
