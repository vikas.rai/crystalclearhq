﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace CrystalClearHQ
{
    public partial class frmTradingPartnersList : Form
    {
        public frmTradingPartnersList()
        {
            InitializeComponent();
        }

        private SqlConnection oConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ClearEdi"].ConnectionString);

        private void frmTradingPartnersList_Load(object sender, EventArgs e)
        {
            string sMyCompanyName = ConfigurationManager.AppSettings["MyCompanyName"];
            this.Text = sMyCompanyName + " Trading Partners";

            oConnection.Open();

            string sSql = "select * from [TradingPartners]";
            SqlDataAdapter oDaTp = new SqlDataAdapter(sSql, oConnection);
            DataSet oTpDs = new DataSet("ds");
            oDaTp.Fill(oTpDs, "ds");
            dgvTradingPartners.DataSource = oTpDs.Tables["ds"];

            oConnection.Close();

        }

        private void dgvTradingPartners_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                frmTradingPartnerProfile ofrmTradingPartner = new frmTradingPartnerProfile();

                DataGridViewRow row = this.dgvTradingPartners.Rows[e.RowIndex];

                ofrmTradingPartner.sTradingPartnerKey = row.Cells["TradingPartnerKey"].Value.ToString();

                ofrmTradingPartner.btnSave.Visible = true;

                ofrmTradingPartner.btnDelete.Visible = false;

                ofrmTradingPartner.Text = "Edit Trading Partner";

                ofrmTradingPartner.Show();

            }
        }

       
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            int nRowIndex = dgvTradingPartners.CurrentRow.Index;

            if (nRowIndex >= 0)
            {
                frmTradingPartnerProfile ofrmTradingPartner = new frmTradingPartnerProfile();

                DataGridViewRow row = this.dgvTradingPartners.Rows[nRowIndex];

                ofrmTradingPartner.sTradingPartnerKey = row.Cells["TradingPartnerKey"].Value.ToString();

                ofrmTradingPartner.btnSave.Visible = true;

                ofrmTradingPartner.btnDelete.Visible = false;

                ofrmTradingPartner.Text = "Edit Trading Partner";

                ofrmTradingPartner.Show();

            }
        }

      
        private void btnDelete_Click(object sender, EventArgs e)
        {
            
        }

        private void btnRefresh_Click_1(object sender, EventArgs e)
        {
            oConnection.Open();

            string sSql = "select * from [TradingPartners]";
            SqlDataAdapter oDaTp = new SqlDataAdapter(sSql, oConnection);
            DataSet oTpDs = new DataSet("ds");
            oDaTp.Fill(oTpDs, "ds");
            dgvTradingPartners.DataSource = oTpDs.Tables["ds"];

            oConnection.Close();
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            frmTradingPartnerProfile ofrmTradingPartner = new frmTradingPartnerProfile();

            ofrmTradingPartner.sTradingPartnerKey = "0";

            ofrmTradingPartner.btnSave.Visible = true;

            ofrmTradingPartner.btnDelete.Visible = false;

            ofrmTradingPartner.Text = "Add New Trading Partner";

            ofrmTradingPartner.Show();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            int nRowIndex = dgvTradingPartners.CurrentRow.Index;

            if (nRowIndex >= 0)
            {
                frmTradingPartnerProfile ofrmTradingPartner = new frmTradingPartnerProfile();

                DataGridViewRow row = this.dgvTradingPartners.Rows[nRowIndex];

                ofrmTradingPartner.sTradingPartnerKey = row.Cells["TradingPartnerKey"].Value.ToString();

                ofrmTradingPartner.btnSave.Visible = false;

                ofrmTradingPartner.btnDelete.Visible = true;

                ofrmTradingPartner.Text = "Delete Trading Partner";

                ofrmTradingPartner.Show();

            }
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            Close();
        }
    }
}
