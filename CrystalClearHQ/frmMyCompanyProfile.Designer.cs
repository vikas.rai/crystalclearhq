﻿namespace CrystalClearHQ
{
    partial class frmMyCompanyProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label15 = new System.Windows.Forms.Label();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtReleaseIndicator = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtRepeatSeparator = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtElementTerminator = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtSegmentTerminator = new System.Windows.Forms.TextBox();
            this.cmbCertificateStoreLocation = new System.Windows.Forms.ComboBox();
            this.cmbCertificateStoreName = new System.Windows.Forms.ComboBox();
            this.cmbCryptographicServiceProvider = new System.Windows.Forms.ComboBox();
            this.cmbCertificateSubjName = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCompanyDomain = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAs2MessageID = new System.Windows.Forms.TextBox();
            this.txtAs2CompanyID = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtUsageIndicator = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtComponentSeparator = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtControlNumber = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAckRequested = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEdiCompanyId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEdiCompanyIdQlfr = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(82, 26);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 13);
            this.label15.TabIndex = 171;
            this.label15.Text = "Company Name:";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Location = new System.Drawing.Point(169, 23);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(225, 20);
            this.txtCompanyName.TabIndex = 134;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(249, 234);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(93, 13);
            this.label17.TabIndex = 170;
            this.label17.Text = "Release Indicator:";
            // 
            // txtReleaseIndicator
            // 
            this.txtReleaseIndicator.Location = new System.Drawing.Point(345, 231);
            this.txtReleaseIndicator.Name = "txtReleaseIndicator";
            this.txtReleaseIndicator.Size = new System.Drawing.Size(49, 20);
            this.txtReleaseIndicator.TabIndex = 144;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(248, 208);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(94, 13);
            this.label16.TabIndex = 169;
            this.label16.Text = "Repeat Separator:";
            // 
            // txtRepeatSeparator
            // 
            this.txtRepeatSeparator.Location = new System.Drawing.Point(345, 205);
            this.txtRepeatSeparator.Name = "txtRepeatSeparator";
            this.txtRepeatSeparator.Size = new System.Drawing.Size(49, 20);
            this.txtRepeatSeparator.TabIndex = 143;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(242, 182);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 13);
            this.label14.TabIndex = 168;
            this.label14.Text = "Element Terminator:";
            // 
            // txtElementTerminator
            // 
            this.txtElementTerminator.Location = new System.Drawing.Point(345, 179);
            this.txtElementTerminator.Name = "txtElementTerminator";
            this.txtElementTerminator.Size = new System.Drawing.Size(49, 20);
            this.txtElementTerminator.TabIndex = 141;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(37, 208);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(131, 13);
            this.label18.TabIndex = 167;
            this.label18.Text = "Data Segment Terminator:";
            // 
            // txtSegmentTerminator
            // 
            this.txtSegmentTerminator.Location = new System.Drawing.Point(168, 205);
            this.txtSegmentTerminator.Name = "txtSegmentTerminator";
            this.txtSegmentTerminator.Size = new System.Drawing.Size(49, 20);
            this.txtSegmentTerminator.TabIndex = 142;
            // 
            // cmbCertificateStoreLocation
            // 
            this.cmbCertificateStoreLocation.ItemHeight = 13;
            this.cmbCertificateStoreLocation.Location = new System.Drawing.Point(168, 381);
            this.cmbCertificateStoreLocation.Name = "cmbCertificateStoreLocation";
            this.cmbCertificateStoreLocation.Size = new System.Drawing.Size(226, 21);
            this.cmbCertificateStoreLocation.TabIndex = 152;
            this.cmbCertificateStoreLocation.Text = "CurrentUser";
            // 
            // cmbCertificateStoreName
            // 
            this.cmbCertificateStoreName.ItemHeight = 13;
            this.cmbCertificateStoreName.Location = new System.Drawing.Point(168, 407);
            this.cmbCertificateStoreName.Name = "cmbCertificateStoreName";
            this.cmbCertificateStoreName.Size = new System.Drawing.Size(226, 21);
            this.cmbCertificateStoreName.TabIndex = 153;
            this.cmbCertificateStoreName.Text = "My";
            // 
            // cmbCryptographicServiceProvider
            // 
            this.cmbCryptographicServiceProvider.ItemHeight = 13;
            this.cmbCryptographicServiceProvider.Location = new System.Drawing.Point(168, 355);
            this.cmbCryptographicServiceProvider.Name = "cmbCryptographicServiceProvider";
            this.cmbCryptographicServiceProvider.Size = new System.Drawing.Size(226, 21);
            this.cmbCryptographicServiceProvider.TabIndex = 150;
            this.cmbCryptographicServiceProvider.Text = "Microsoft Strong Cryptographic Provider";
            // 
            // cmbCertificateSubjName
            // 
            this.cmbCertificateSubjName.Location = new System.Drawing.Point(168, 433);
            this.cmbCertificateSubjName.Name = "cmbCertificateSubjName";
            this.cmbCertificateSubjName.Size = new System.Drawing.Size(226, 21);
            this.cmbCertificateSubjName.TabIndex = 155;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(50, 410);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(116, 13);
            this.label13.TabIndex = 164;
            this.label13.Text = "Certificate Store Name:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(37, 384);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(129, 13);
            this.label12.TabIndex = 163;
            this.label12.Text = "Certificate Store Location:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 358);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(156, 13);
            this.label10.TabIndex = 162;
            this.label10.Text = "Cryptographic Service Provider:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(38, 436);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(127, 13);
            this.label9.TabIndex = 161;
            this.label9.Text = "Certificate Subject Name:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(50, 332);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 13);
            this.label7.TabIndex = 160;
            this.label7.Text = "Company Msg Domain:";
            // 
            // txtCompanyDomain
            // 
            this.txtCompanyDomain.Location = new System.Drawing.Point(168, 329);
            this.txtCompanyDomain.Name = "txtCompanyDomain";
            this.txtCompanyDomain.Size = new System.Drawing.Size(226, 20);
            this.txtCompanyDomain.TabIndex = 149;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(75, 306);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 159;
            this.label4.Text = "AS2 Message ID:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(74, 280);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 158;
            this.label3.Text = "AS2 Company ID:";
            // 
            // txtAs2MessageID
            // 
            this.txtAs2MessageID.Location = new System.Drawing.Point(168, 303);
            this.txtAs2MessageID.Name = "txtAs2MessageID";
            this.txtAs2MessageID.Size = new System.Drawing.Size(226, 20);
            this.txtAs2MessageID.TabIndex = 148;
            // 
            // txtAs2CompanyID
            // 
            this.txtAs2CompanyID.Location = new System.Drawing.Point(168, 277);
            this.txtAs2CompanyID.Name = "txtAs2CompanyID";
            this.txtAs2CompanyID.Size = new System.Drawing.Size(226, 20);
            this.txtAs2CompanyID.TabIndex = 146;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(238, 486);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(123, 35);
            this.btnCancel.TabIndex = 166;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(73, 486);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(123, 35);
            this.btnSave.TabIndex = 165;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(80, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 157;
            this.label5.Text = "Usage Indicator:";
            // 
            // txtUsageIndicator
            // 
            this.txtUsageIndicator.Location = new System.Drawing.Point(168, 153);
            this.txtUsageIndicator.Name = "txtUsageIndicator";
            this.txtUsageIndicator.Size = new System.Drawing.Size(226, 20);
            this.txtUsageIndicator.TabIndex = 139;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(52, 182);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 13);
            this.label6.TabIndex = 156;
            this.label6.Text = "Component Separator:";
            // 
            // txtComponentSeparator
            // 
            this.txtComponentSeparator.Location = new System.Drawing.Point(168, 179);
            this.txtComponentSeparator.Name = "txtComponentSeparator";
            this.txtComponentSeparator.Size = new System.Drawing.Size(49, 20);
            this.txtComponentSeparator.TabIndex = 140;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(79, 104);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 13);
            this.label11.TabIndex = 154;
            this.label11.Text = "Control Number: ";
            // 
            // txtControlNumber
            // 
            this.txtControlNumber.Location = new System.Drawing.Point(168, 101);
            this.txtControlNumber.Name = "txtControlNumber";
            this.txtControlNumber.Size = new System.Drawing.Size(226, 20);
            this.txtControlNumber.TabIndex = 137;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(81, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 13);
            this.label8.TabIndex = 151;
            this.label8.Text = "Ack Requested:";
            // 
            // txtAckRequested
            // 
            this.txtAckRequested.Location = new System.Drawing.Point(168, 127);
            this.txtAckRequested.Name = "txtAckRequested";
            this.txtAckRequested.Size = new System.Drawing.Size(226, 20);
            this.txtAckRequested.TabIndex = 138;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(75, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 147;
            this.label2.Text = "EDI Company ID:";
            // 
            // txtEdiCompanyId
            // 
            this.txtEdiCompanyId.Location = new System.Drawing.Point(168, 75);
            this.txtEdiCompanyId.Name = "txtEdiCompanyId";
            this.txtEdiCompanyId.Size = new System.Drawing.Size(226, 20);
            this.txtEdiCompanyId.TabIndex = 136;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 145;
            this.label1.Text = "EDI Company ID Qlfr:";
            // 
            // txtEdiCompanyIdQlfr
            // 
            this.txtEdiCompanyIdQlfr.Location = new System.Drawing.Point(168, 49);
            this.txtEdiCompanyIdQlfr.Name = "txtEdiCompanyIdQlfr";
            this.txtEdiCompanyIdQlfr.Size = new System.Drawing.Size(226, 20);
            this.txtEdiCompanyIdQlfr.TabIndex = 135;
            // 
            // frmMyCompanyProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 522);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtCompanyName);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtReleaseIndicator);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtRepeatSeparator);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtElementTerminator);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtSegmentTerminator);
            this.Controls.Add(this.cmbCertificateStoreLocation);
            this.Controls.Add(this.cmbCertificateStoreName);
            this.Controls.Add(this.cmbCryptographicServiceProvider);
            this.Controls.Add(this.cmbCertificateSubjName);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtCompanyDomain);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtAs2MessageID);
            this.Controls.Add(this.txtAs2CompanyID);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtUsageIndicator);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtComponentSeparator);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtControlNumber);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtAckRequested);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtEdiCompanyId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtEdiCompanyIdQlfr);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmMyCompanyProfile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMyCompanyProfile";
            this.Load += new System.EventHandler(this.frmMyCompanyProfile_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtReleaseIndicator;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtRepeatSeparator;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtElementTerminator;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtSegmentTerminator;
        internal System.Windows.Forms.ComboBox cmbCertificateStoreLocation;
        internal System.Windows.Forms.ComboBox cmbCertificateStoreName;
        internal System.Windows.Forms.ComboBox cmbCryptographicServiceProvider;
        internal System.Windows.Forms.ComboBox cmbCertificateSubjName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCompanyDomain;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAs2MessageID;
        private System.Windows.Forms.TextBox txtAs2CompanyID;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtUsageIndicator;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtComponentSeparator;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtControlNumber;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAckRequested;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtEdiCompanyId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEdiCompanyIdQlfr;
    }
}