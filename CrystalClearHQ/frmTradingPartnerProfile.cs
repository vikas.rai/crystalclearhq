﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using Edidev.FrameworkEDIx64;

namespace CrystalClearHQ
{
    public partial class frmTradingPartnerProfile : Form
    {
        public frmTradingPartnerProfile()
        {
            InitializeComponent();
        }

        public string sTradingPartnerKey;

        private SqlConnection oConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ClearEdi"].ConnectionString);

        private void frmTradingPartner_Load(object sender, EventArgs e)
        {

            oConnection.Open();

            string sSql = "select * from [TradingPartners] where TradingPartnerKey = " + sTradingPartnerKey;
            SqlDataAdapter oDaTp = new SqlDataAdapter(sSql, oConnection);
            DataSet oTpDs = new DataSet("ds");
            oDaTp.Fill(oTpDs, "ds");

            foreach (DataRow oTpRow in oTpDs.Tables["ds"].Rows)
            {

                txtCompanyName.Text = oTpRow["CompanyName"].ToString();
                EDI_CompanyIdQlfr.Text = oTpRow["EDI_CompanyIdQlfr"].ToString();
                txtEDI_CompanyId.Text = oTpRow["EDI_CompanyId"].ToString();
                txtAS2_CompanyId.Text = oTpRow["AS2_CompanyId"].ToString();
                txtCompanySefFolder.Text = oTpRow["CompanySefFolder"].ToString();
                txtCompanyInboundFolder.Text = oTpRow["CompanyInboundFolder"].ToString();
                txtCompanyOutboundFolder.Text = oTpRow["CompanyOutboundFolder"].ToString();
                txtCompanyRejectedFolder.Text = oTpRow["CompanyRejectedFolder"].ToString();
                txtCompanyDoneFolder.Text = oTpRow["CompanyDoneFolder"].ToString();
                txtCompany997Folder.Text = oTpRow["Company997Folder"].ToString();
                txtSentFolder.Text = oTpRow["SentFolder"].ToString();
                txtFailedFolder.Text = oTpRow["FailedFolder"].ToString();
                txtAS2_Folder.Text = oTpRow["AS2_Folder"].ToString();
                cmbCertificateSubjName.Text = oTpRow["CertificateSubjName"].ToString();
                cmbCryptographicServiceProvider.Text = oTpRow["CryptographicServiceProvider"].ToString();
                cmbCertificateStoreName.Text = oTpRow["CertificateStoreName"].ToString();
                cmbCertificateStoreLocation.Text = oTpRow["CertificateStoreLocation"].ToString();
                cmbEncryptionAlgorithm.Text = oTpRow["EncryptionAlgorithm"].ToString();
                cmbSigningHash.Text = oTpRow["SigningHash"].ToString();
                txtContentType.Text = oTpRow["ContentType"].ToString();
                txtTargetURL.Text = oTpRow["Target_URL"].ToString();
                txtDispositionNotificationTo.Text = oTpRow["DispositionNotificationTo"].ToString();
                txtReceiptDeliveryOption.Text = oTpRow["ReceiptDeliveryOption"].ToString();
                ckEnableEncryption.Checked = oTpRow["EnableEncryption"] == null ? false : Convert.ToBoolean(Convert.ToInt32(oTpRow["EnableEncryption"]));
                ckEnableAssurance.Checked = oTpRow["EnableAssurance"] == null ? false : Convert.ToBoolean(Convert.ToInt32(oTpRow["EnableAssurance"]));
                ckEnableCompression.Checked = oTpRow["EnableCompression"] == null ? false : Convert.ToBoolean(Convert.ToInt32(oTpRow["EnableCompression"]));
                ckBase64Encoding.Checked = oTpRow["Base64Encoding"] == null ? false : Convert.ToBoolean(Convert.ToInt32(oTpRow["Base64Encoding"]));
                ckReceiveMDN.Checked = oTpRow["ReceiveMDN"] == null ? false : Convert.ToBoolean(Convert.ToInt32(oTpRow["ReceiveMDN"]));
                ckReceiveSignedMDN.Checked = oTpRow["ReceiveSignedMDN"] == null ? false : Convert.ToBoolean(Convert.ToInt32(oTpRow["ReceiveSignedMDN"]));
                ckSynchronous.Checked = oTpRow["Synchronous"] == null ? false : Convert.ToBoolean(Convert.ToInt32(oTpRow["Synchronous"]));
                ckTa1AckRequest.Checked = oTpRow["AckTA1Requested"] == null ? false : Convert.ToBoolean(Convert.ToInt32(oTpRow["AckTA1Requested"]));
                ck997AckRequest.Checked = oTpRow["Ack997Requested"] == null ? false : Convert.ToBoolean(Convert.ToInt32(oTpRow["Ack997Requested"]));

                this.Text = txtCompanyName.Text.Trim() + " Configuration";

            }

            oConnection.Close();

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

            
        }

        private void btnSefFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

            folderBrowserDialog1.SelectedPath = AppDomain.CurrentDomain.BaseDirectory + txtCompanySefFolder.Text;
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtCompanySefFolder.Text = folderBrowserDialog1.SelectedPath.Replace(AppDomain.CurrentDomain.BaseDirectory, "") + @"\";

            }
        }

        private void cmbCryptographicServiceProvider_DropDown(object sender, EventArgs e)
        {
            if (cmbCryptographicServiceProvider.Items.Count == 0)
            {
                ediDocument oEdiDoc = new ediDocument();
                ediSecurities oSecurities = oEdiDoc.GetSecurities();
                ediSecurityServiceProviders oServProvs = oSecurities.GetServiceProviders();
                ediSecurityServiceProvider oServProv = oServProvs.First();

                for (int i = 1; i <= oServProvs.Count; i++)
                {
                    cmbCryptographicServiceProvider.Items.Add(oServProv.Name);
                    ediSecurityServiceProvider.Set(ref oServProv, oServProvs.Next());
                }

                if (cmbCryptographicServiceProvider.Text.Trim().Length == 0)
                {
                    cmbCryptographicServiceProvider.Text = "Microsoft Strong Cryptographic Provider";
                }

                if (oServProv != null)
                    oServProv.Dispose();

                if (oServProvs != null)
                    oServProvs.Dispose();

                if (oSecurities != null)
                    oSecurities.Dispose();

                if (oEdiDoc != null)
                    oEdiDoc.Dispose();
            }
        }

        private void cmbEncryptionAlgorithm_DropDown(object sender, EventArgs e)
        {
            if (cmbEncryptionAlgorithm.Items.Count == 0)
            {
                ediDocument oEdiDoc = new ediDocument();
                ediSecurities oSecurities = oEdiDoc.GetSecurities();
                ediSecurityAlgorithm oAlgorithm = null;
                ediSecurityServiceProviders oServProvs = oSecurities.GetServiceProviders();
                ediSecurityServiceProvider oServProv = oServProvs.GetServiceProviderByName(cmbCryptographicServiceProvider.Text);
                ediSecurityAlgorithms oAlgorithms = oServProv.GetAlgorithms();

                for (int i = 1; i <= oAlgorithms.Count; i++)
                {
                    ediSecurityAlgorithm.Set(ref oAlgorithm, oAlgorithms.GetAlgorithmByIndex(i));
                    if (oAlgorithm.Type == "Encrypt")
                    {
                        cmbEncryptionAlgorithm.Items.Add(oAlgorithm.Name);
                    }
                }

                if (oAlgorithm != null)
                    oAlgorithm.Dispose();

                if (oAlgorithms != null)
                    oAlgorithms.Dispose();

                if (oServProv != null)
                    oServProv.Dispose();

                if (oServProvs != null)
                    oServProvs.Dispose();

                if (oSecurities != null)
                    oSecurities.Dispose();

                if (oEdiDoc != null)
                    oEdiDoc.Dispose();

            }
        }

        private void cmbSigningHash_DropDown(object sender, EventArgs e)
        {
            if (cmbSigningHash.Items.Count == 0)
            {
                ediDocument oEdiDoc = new ediDocument();
                ediSecurities oSecurities = oEdiDoc.GetSecurities();
                ediSecurityAlgorithm oAlgorithm = null;
                ediSecurityServiceProviders oServProvs = oSecurities.GetServiceProviders();
                ediSecurityServiceProvider oServProv = oServProvs.GetServiceProviderByName(cmbCryptographicServiceProvider.Text);
                ediSecurityAlgorithms oAlgorithms = oServProv.GetAlgorithms();

                for (int i = 1; i <= oAlgorithms.Count; i++)
                {
                    ediSecurityAlgorithm.Set(ref oAlgorithm, oAlgorithms.GetAlgorithmByIndex(i));
                    if (oAlgorithm.Type == "Hash")
                    {
                        cmbSigningHash.Items.Add(oAlgorithm.Name);
                    }
                }

                if (oAlgorithm != null)
                    oAlgorithm.Dispose();

                if (oAlgorithms != null)
                    oAlgorithms.Dispose();

                if (oServProv != null)
                    oServProv.Dispose();

                if (oServProvs != null)
                    oServProvs.Dispose();

                if (oSecurities != null)
                    oSecurities.Dispose();

                if (oEdiDoc != null)
                    oEdiDoc.Dispose();

            }
        }

        private void cmbCertificateStoreLocation_DropDown(object sender, EventArgs e)
        {
            if (cmbCertificateStoreLocation.Items.Count == 0)
            {
                ediDocument oEdiDoc = new ediDocument();
                ediSecurities oSecurities = oEdiDoc.GetSecurities();
                ediSecurityCertStoreLocations oCertLocs = oSecurities.GetCertificateStoreLocations();
                ediSecurityCertificateStores oCertStores = oCertLocs.GetFirstCertificateStores();

                while (oCertStores != null)
                {

                    cmbCertificateStoreLocation.Items.Add(oCertStores.Location);

                    oCertStores = oCertLocs.GetNextCertificateStores();
                }

                if (cmbCertificateStoreLocation.Text.Trim().Length == 0)
                {
                    cmbCertificateStoreLocation.Text = "CurrentUser";
                }

                if (oCertStores != null)
                    oCertStores.Dispose();

                if (oCertLocs != null)
                    oCertLocs.Dispose();

                if (oSecurities != null)
                    oSecurities.Dispose();

                if (oEdiDoc != null)
                    oEdiDoc.Dispose();

            }
        }

        private void cmbCertificateStoreName_DropDown(object sender, EventArgs e)
        {
            if (cmbCertificateStoreName.Items.Count == 0)
            {
                ediDocument oEdiDoc = new ediDocument();
                ediSecurities oSecurities = oEdiDoc.GetSecurities();
                ediSecurityCertStoreLocations oCertLocs = oSecurities.GetCertificateStoreLocations();
                ediSecurityCertificateStores oCertStores = oCertLocs.GetCertificateStores(cmbCertificateStoreLocation.Text);
                ediSecurityCertificateStore oCertStore = oCertStores.GetFirstCertificateStore();

                for (int i = 1; i <= oCertStores.Count; i++)
                {
                    cmbCertificateStoreName.Items.Add(oCertStore.StoreName);
                    ediSecurityCertificateStore.Set(ref oCertStore, oCertStores.GetNextCertificateStore());
                }

                if (cmbCertificateStoreName.Text.Trim().Length == 0)
                {
                    cmbCertificateStoreName.Text = "My";
                }

                if (oCertStore != null)
                    oCertStore.Dispose();

                if (oCertStores != null)
                    oCertStores.Dispose();

                if (oCertLocs != null)
                    oCertLocs.Dispose();

                if (oSecurities != null)
                    oSecurities.Dispose();

                if (oEdiDoc != null)
                    oEdiDoc.Dispose();
            }
        }

        private void cmbCertificateSubjName_DropDown(object sender, EventArgs e)
        {
            if (cmbCertificateSubjName.Items.Count == 0)
            {
                //List all the certificates in a certificate store

                ediDocument oEdiDoc = new ediDocument();
                ediSecurities oSecurities = oEdiDoc.GetSecurities();
                ediSecurityCertStoreLocations oCertLoc = oSecurities.GetCertificateStoreLocations();
                ediSecurityCertificateStores oCertStores = oCertLoc.GetCertificateStores(cmbCertificateStoreLocation.Text);
                ediSecurityCertificateStore oCertStore = oCertStores.GetCertificateStore(cmbCertificateStoreName.Text);

                cmbCertificateSubjName.Items.Clear();

                ediSecurityCertificate oCert = oCertStore.GetFirstCertificate();

                for (int i = 1; i <= oCertStore.Count; i++)
                {
                    cmbCertificateSubjName.Items.Add(oCert.SubjectName);
                    ediSecurityCertificate.Set(ref oCert, oCertStore.GetNextCertificate());
                }

                if (oCertStore != null)
                    oCertStore.Dispose();

                if (oCertStores != null)
                    oCertStores.Dispose();

                if (oCertLoc != null)
                    oCertLoc.Dispose();

                if (oSecurities != null)
                    oSecurities.Dispose();

                if (oEdiDoc != null)
                    oEdiDoc.Dispose();

            }
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            string sSql;

            oConnection.Open();

            SqlDataAdapter oDaTp = new SqlDataAdapter();

            sSql = "delete [TradingPartners] where TradingPartnerKey = " + sTradingPartnerKey;
            oDaTp.DeleteCommand = new SqlCommand(sSql, oConnection);
            oDaTp.DeleteCommand.ExecuteNonQuery();

            MessageBox.Show("Record Deleted");

            Close();
        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            string sSql;

            oConnection.Open();

            SqlDataAdapter oDaTp = new SqlDataAdapter();

            if (sTradingPartnerKey != "0")
            {
                sSql = @"UPDATE [TradingPartners] SET CompanyName = @CompanyName, EDI_CompanyIdQlfr = @EDI_CompanyIdQlfr,
                        EDI_CompanyId = @EDI_CompanyId, AS2_CompanyId = @AS2_CompanyId, CompanySefFolder = @CompanySefFolder,
                        CompanyInboundFolder = @CompanyInboundFolder, CompanyOutboundFolder = @CompanyOutboundFolder, 
                        CompanyRejectedFolder = @CompanyRejectedFolder, CompanyDoneFolder = @CompanyDoneFolder, Company997Folder = @Company997Folder, 
                        SentFolder = @SentFolder, FailedFolder = @FailedFolder,
                        AS2_Folder = @AS2_Folder, CertificateSubjName = @CertificateSubjName,
                        CryptographicServiceProvider = @CryptographicServiceProvider, CertificateStoreName = @CertificateStoreName,
                        CertificateStoreLocation = @CertificateStoreLocation, EncryptionAlgorithm = @EncryptionAlgorithm,
                        SigningHash = @SigningHash, Target_URL = @Target_URL, ContentType = @ContentType,
                        DispositionNotificationTo = @DispositionNotificationTo, ReceiptDeliveryOption = @ReceiptDeliveryOption,
                        EnableEncryption = @EnableEncryption, EnableAssurance = @EnableAssurance, EnableCompression = @EnableCompression, 
                        Base64Encoding = @Base64Encoding, ReceiveMDN = @ReceiveMDN, ReceiveSignedMDN = @ReceiveSignedMDN, Synchronous = @Synchronous,
                        AckTA1Requested = @AckTA1Requested, Ack997Requested = @Ack997Requested 
                        where TradingPartnerKey = @TradingPartnerKey";

                oDaTp.UpdateCommand = new SqlCommand(sSql, oConnection);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@CompanyName", txtCompanyName.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@EDI_CompanyIdQlfr", EDI_CompanyIdQlfr.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@EDI_CompanyId", txtEDI_CompanyId.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@AS2_CompanyId", txtAS2_CompanyId.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@CompanySefFolder", txtCompanySefFolder.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@CompanyInboundFolder", txtCompanyInboundFolder.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@CompanyOutboundFolder", txtCompanyOutboundFolder.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@CompanyRejectedFolder", txtCompanyRejectedFolder.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@CompanyDoneFolder", txtCompanyDoneFolder.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@Company997Folder", txtCompany997Folder.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@SentFolder", txtSentFolder.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@FailedFolder", txtFailedFolder.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@AS2_Folder", txtAS2_Folder.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@CertificateSubjName", cmbCertificateSubjName.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@CryptographicServiceProvider", cmbCryptographicServiceProvider.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@CertificateStoreName", cmbCertificateStoreName.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@CertificateStoreLocation", cmbCertificateStoreLocation.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@EncryptionAlgorithm", cmbEncryptionAlgorithm.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@SigningHash", cmbSigningHash.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@ContentType", txtContentType.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@Target_URL", txtTargetURL.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@DispositionNotificationTo", txtDispositionNotificationTo.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@ReceiptDeliveryOption", txtReceiptDeliveryOption.Text);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@EnableEncryption", ckEnableEncryption.Checked);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@EnableAssurance", ckEnableAssurance.Checked);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@EnableCompression", ckEnableCompression.Checked);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@Base64Encoding", ckBase64Encoding.Checked);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@ReceiveMDN", ckReceiveMDN.Checked);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@ReceiveSignedMDN", ckReceiveSignedMDN.Checked);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@Synchronous", ckSynchronous.Checked);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@AckTA1Requested", ckTa1AckRequest.Checked);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@Ack997Requested", ck997AckRequest.Checked);
                oDaTp.UpdateCommand.Parameters.AddWithValue("@TradingPartnerKey", sTradingPartnerKey);
                oDaTp.UpdateCommand.ExecuteNonQuery();
            }
            else
            {
                sSql = @"INSERT INTO [TradingPartners] (CompanyName, EDI_CompanyIdQlfr, EDI_CompanyId, AS2_CompanyId, CompanySefFolder,  
                        CompanyInboundFolder, CompanyOutboundFolder, CompanyRejectedFolder, CompanyDoneFolder, Company997Folder, SentFolder,
                        FailedFolder, AS2_Folder, CertificateSubjName, CryptographicServiceProvider, CertificateStoreName, CertificateStoreLocation,
                        EncryptionAlgorithm, SigningHash, ContentType, Target_URL, DispositionNotificationTo, ReceiptDeliveryOption, EnableEncryption, 
                        EnableAssurance, EnableCompression, Base64Encoding, ReceiveMDN, ReceiveSignedMDN, Synchronous, AckTA1Requested, Ack997Requested ) 
                        values 
                        (@CompanyName, @EDI_CompanyIdQlfr, @EDI_CompanyId, @AS2_CompanyId, @CompanySefFolder,  
                        @CompanyInboundFolder, @CompanyOutboundFolder, @CompanyRejectedFolder, @CompanyDoneFolder, @Company997Folder, @SentFolder, 
                        @FailedFolder, @AS2_Folder, @CertificateSubjName, @CryptographicServiceProvider, @CertificateStoreName, @CertificateStoreLocation, 
                        @EncryptionAlgorithm, @SigningHash, @ContentType, @Target_URL, @DispositionNotificationTo, @ReceiptDeliveryOption, @EnableEncryption, 
                        @EnableAssurance, @EnableCompression, @Base64Encoding, @ReceiveMDN, @ReceiveSignedMDN, @Synchronous, @AckTA1Requested, @Ack997Requested);
                        SELECT scope_identity()";

                oDaTp.InsertCommand = new SqlCommand(sSql, oConnection);
                oDaTp.InsertCommand.Parameters.AddWithValue("@CompanyName", txtCompanyName.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@EDI_CompanyIdQlfr", EDI_CompanyIdQlfr.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@EDI_CompanyId", txtEDI_CompanyId.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@AS2_CompanyId", txtAS2_CompanyId.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@CompanySefFolder", txtCompanySefFolder.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@CompanyInboundFolder", txtCompanyInboundFolder.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@CompanyOutboundFolder", txtCompanyOutboundFolder.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@CompanyRejectedFolder", txtCompanyRejectedFolder.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@CompanyDoneFolder", txtCompanyDoneFolder.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@Company997Folder", txtCompany997Folder.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@SentFolder", txtSentFolder.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@FailedFolder", txtFailedFolder.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@AS2_Folder", txtAS2_Folder.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@CertificateSubjName", cmbCertificateSubjName.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@CryptographicServiceProvider", cmbCryptographicServiceProvider.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@CertificateStoreName", cmbCertificateStoreName.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@CertificateStoreLocation", cmbCertificateStoreLocation.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@EncryptionAlgorithm", cmbEncryptionAlgorithm.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@SigningHash", cmbSigningHash.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@ContentType", txtContentType.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@Target_URL", txtTargetURL.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@DispositionNotificationTo", txtDispositionNotificationTo.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@ReceiptDeliveryOption", txtReceiptDeliveryOption.Text);
                oDaTp.InsertCommand.Parameters.AddWithValue("@EnableEncryption", ckEnableEncryption.Checked);
                oDaTp.InsertCommand.Parameters.AddWithValue("@EnableAssurance", ckEnableAssurance.Checked);
                oDaTp.InsertCommand.Parameters.AddWithValue("@EnableCompression", ckEnableCompression.Checked);
                oDaTp.InsertCommand.Parameters.AddWithValue("@Base64Encoding", ckBase64Encoding.Checked);
                oDaTp.InsertCommand.Parameters.AddWithValue("@ReceiveMDN", ckReceiveMDN.Checked);
                oDaTp.InsertCommand.Parameters.AddWithValue("@ReceiveSignedMDN", ckReceiveSignedMDN.Checked);
                oDaTp.InsertCommand.Parameters.AddWithValue("@Synchronous", ckSynchronous.Checked);
                oDaTp.InsertCommand.Parameters.AddWithValue("@AckTA1Requested", ckTa1AckRequest.Checked);
                oDaTp.InsertCommand.Parameters.AddWithValue("@Ack997Requested", ck997AckRequest.Checked);
                Int32 nKey = (Int32)(decimal)oDaTp.InsertCommand.ExecuteScalar();

                sTradingPartnerKey = nKey.ToString();

            }

            oConnection.Close();

            MessageBox.Show("Save Successful");

            Close();
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void frmTradingPartnerProfile_Load(object sender, EventArgs e)
        {

        }

        private void btnSefFolder_Click_1(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

            folderBrowserDialog1.SelectedPath = AppDomain.CurrentDomain.BaseDirectory + txtCompanySefFolder.Text;
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtCompanySefFolder.Text = folderBrowserDialog1.SelectedPath.Replace(AppDomain.CurrentDomain.BaseDirectory, "") + @"\";
            }
        }

        private void btnInboundFolder_Click_1(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

            folderBrowserDialog1.SelectedPath = AppDomain.CurrentDomain.BaseDirectory + txtCompanyInboundFolder.Text;
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtCompanyInboundFolder.Text = folderBrowserDialog1.SelectedPath.Replace(AppDomain.CurrentDomain.BaseDirectory, "") + @"\";

            }
        }

        private void btnOutboundFolder_Click_1(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

            folderBrowserDialog1.SelectedPath = AppDomain.CurrentDomain.BaseDirectory + txtCompanyOutboundFolder.Text;
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtCompanyOutboundFolder.Text = folderBrowserDialog1.SelectedPath.Replace(AppDomain.CurrentDomain.BaseDirectory, "") + @"\";

            }
        }

        private void btnRejectedFolder_Click_1(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

            folderBrowserDialog1.SelectedPath = AppDomain.CurrentDomain.BaseDirectory + txtCompanyRejectedFolder.Text;
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtCompanyRejectedFolder.Text = folderBrowserDialog1.SelectedPath.Replace(AppDomain.CurrentDomain.BaseDirectory, "") + @"\";

            }
        }

        private void btnDoneFolder_Click_1(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

            folderBrowserDialog1.SelectedPath = AppDomain.CurrentDomain.BaseDirectory + txtCompanyDoneFolder.Text;
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtCompanyDoneFolder.Text = folderBrowserDialog1.SelectedPath.Replace(AppDomain.CurrentDomain.BaseDirectory, "") + @"\";

            }
        }

        private void btn997Folder_Click_1(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

            folderBrowserDialog1.SelectedPath = AppDomain.CurrentDomain.BaseDirectory + txtCompany997Folder.Text;
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtCompany997Folder.Text = folderBrowserDialog1.SelectedPath.Replace(AppDomain.CurrentDomain.BaseDirectory, "") + @"\";

            }
        }

        private void btnAs2SentFolder_Click_1(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

            folderBrowserDialog1.SelectedPath = AppDomain.CurrentDomain.BaseDirectory + txtSentFolder.Text;
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtSentFolder.Text = folderBrowserDialog1.SelectedPath.Replace(AppDomain.CurrentDomain.BaseDirectory, "") + @"\";

            }
        }

        private void btnAs2FailedFolder_Click_1(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

            folderBrowserDialog1.SelectedPath = AppDomain.CurrentDomain.BaseDirectory + txtFailedFolder.Text;
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtFailedFolder.Text = folderBrowserDialog1.SelectedPath.Replace(AppDomain.CurrentDomain.BaseDirectory, "") + @"\";

            }
        }

        private void btnAs2LogFolder_Click_1(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

            folderBrowserDialog1.SelectedPath = AppDomain.CurrentDomain.BaseDirectory + txtAS2_Folder.Text;
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtAS2_Folder.Text = folderBrowserDialog1.SelectedPath.Replace(AppDomain.CurrentDomain.BaseDirectory, "") + @"\";

            }
        }
    }
}
